﻿namespace CallCenterOS.CORE
{
    public enum IncidenceStatus : int
    {
        ///<summary>
        /// Abierta por defecto 0
        ///</summary>
        Open = 0,
        ///<summary>
        /// Cerrado
        ///</summary>
        Closed = 1,     
    }
}