﻿namespace CallCenterOS.CORE
{
    ///<summary>
    /// Enumerado de prioridad
    ///</summary>
    public enum IncidencePriority
    {
        ///<summary>
        /// Baja
        ///</summary>
        Low = 0,
        ///<summary>
        /// Media
        ///</summary>
        Normal = 1,
        ///<summary>
        /// Alta
        ///</summary>
        High = 2, 
    }
}