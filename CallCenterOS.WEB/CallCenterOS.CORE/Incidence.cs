﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace CallCenterOS.CORE

    ///<summary>
    ///Entidad de dominio de Incidencias
    ///</summary>
{
    public class Incidence
    {
        ///<summary>
        /// ID de la incidencia
        ///</summary>     
        public int Id { get; set; }

        ///<summary>
        /// Nombre del Equipo
        ///</summary>     
        public string Equipment { get; set; }

        ///<summary>
        /// Fecha de Creación de incidencia
        ///</summary>      
        public DateTime CreatedDate { get; set; }

        ///<summary>
        /// Fecha de cierre de incidencia
        /// Datetime con ? le indicamos que puede ser NULL su valor.
        ///</summary>
        public DateTime? CloseDate { get; set; }

        ///<summary>
        /// Nota interna de la incidencia
        ///</summary>
        public string InternalNote { get; set; }

        ///<summary>
        ///
        ///</summary>
        public IncidenceStatus Status { get; set; }

        ///<summary>
        ///
        ///</summary>
        public IncidencePriority Priority { get; set; }

        ///<summary>
        /// Tipo de Incidencia
        ///</summary>
        
        public IncidenceType IncidenceType { get; set; }

        ///<summary>
        /// Usuario que crea la incidencia
        ///</summary>
        
        public ApplicationUser User { get; set; }

        ///<summary>
        /// Identificador del usuario que ha creado la incidencia
        ///</summary>    
        [ForeignKey("User")]
        public String User_Id { get; set; }

        ///<summary>
        /// Colección de mensajes
        ///</summary>
        public virtual List<Message> Messages { get; set; }
    }
}
