﻿namespace CallCenterOS.CORE
{
    ///<summary>
    /// Enumerado del tipo de incidencia
    ///</summary>
    public enum IncidenceType
    {
        ///<summary>
        /// Software
        ///</summary>
        Software = 1,
        ///<summary>
        /// Hardware
        ///</summary>
        Hardware = 2,
        ///<summary>
        /// Otros
        ///</summary>
        Others = 3,
    }
}