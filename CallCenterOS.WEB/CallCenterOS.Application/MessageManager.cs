﻿using CallCenterOS.CORE;
using CallCenterOS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenterOS.Application
{
        ///<summary>
        /// Manager de MEssage
        ///</summary>
        ///
        public class MessageManager : GenericManager<Message>
        {
            /// <summary>
            /// Constructor del manager de Message
            /// </summary>
            /// <param name="context">Contexto de datos</param>
            public MessageManager(ApplicationDbContext context) : base(context)
            {

            }
        }
}
