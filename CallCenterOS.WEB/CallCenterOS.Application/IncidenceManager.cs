﻿using CallCenterOS.CORE;
using CallCenterOS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenterOS.Application
{
    /// <summary>
    /// Clase manager de message
    /// </summary>
    public class IncidenceManager : GenericManager<Incidence>
    {
        /// <summary>
        /// Constructor de la clase manager de incidence
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public IncidenceManager(ApplicationDbContext context) : base(context)
        {

        }
        /// <summary>
        /// Método que retorna todas las incidencias de un usuario
        /// </summary>
        /// <param name="userId">Identificados de usuario</param>
        /// <returns>Todas las incidencias de usuario</returns>
        public IQueryable<Incidence> GetByUserId(string userId)
        {
            return Context.Set<Incidence>().Where(e => e.User_Id == userId);
        }

        /// <summary>
        /// Obtiene una incidencia con sus mensajes
        /// </summary>
        /// <param name="id">Identificados de la incidencia</param>
        /// <returns>Incidencia con sus mensajes si existe o null en caso de no existir</returns>
        public Incidence GetByIdAndMessages(int id)
        {
            return Context.Set<Incidence>().Include("Messages").Where(i=> i.Id == id).SingleOrDefault();
        }
    }
}
