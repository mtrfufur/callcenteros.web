﻿using CallCenterOS.CORE;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallCenterOS.DAL
    ///<summary>
    ///Contexto de datos
    ///</summary>
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Constructor por defecto de la clase
        /// </summary>
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        /// <summary>
        /// Método estático para crear el contexto
        /// </summary>
        /// <returns>Contexto de datos</returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// Colección persistible de incidencias
        /// </summary>
        public DbSet<Incidence> Incidences { get; set; }

        ///<summary>
        /// Colección persistible de mensajes
        ///</summary>       
        public DbSet<Message> Messages { get; set; }
    }
}
