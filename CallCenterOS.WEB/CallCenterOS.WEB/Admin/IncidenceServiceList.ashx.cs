﻿using CallCenterOS.Application;
using CallCenterOS.DAL;
using CallCenterOS.Web.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;

namespace CallCenterOS.WEB.Admin
{
    /// <summary>
    /// Descripción breve de IncidenceServiceList
    /// </summary>
    public class IncidenceServiceList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Those parameters are sent by the plugin
            var iDisplayLength = int.Parse(context.Request["iDisplayLength"]);
            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
            var sSearch = context.Request["sSearch"];
            var iSortDir = context.Request["sSortDir_0"];
            var iSortCol = context.Request["iSortCol_0"];
            var sortColum = context.Request.QueryString["mDataProp_" + iSortCol];


            ApplicationDbContext contextdb = new ApplicationDbContext();
            IncidenceManager incidenceManager = new IncidenceManager(contextdb);
            #region select
            var allIncidences = incidenceManager.GetAll();
            var incidences = allIncidences
                    .Select(p => new AdminIncidenceList
                    {
                        Id = p.Id,
                        Message = p.Messages.FirstOrDefault().Text,
                        Date = p.CreatedDate,
                        Status = p.Status.ToString(),
                        User = p.User!=null ? p.User.UserName : ""
                    });
            #endregion
            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"Id.ToString().Contains(@0) ||
                                 Message.ToString().Contains(@0) ||
                                 Date.ToString().Contains(@0) ||
                                 Status.ToString().Contains(@0) ||
                                 User.Tostring().Contains(@0)";
                
                incidences = incidences.Where(where, sSearch);
            }
            #endregion
            #region Paginate
            incidences = incidences
                        .OrderBy(sortColum + " " + iSortDir)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength);
            #endregion
            var result = new
            {
                iTotalRecords = allIncidences.Count(),
                iTotalDisplayRecords = allIncidences.Count(),
                aaData = incidences
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}