﻿using CallCenterOS.CORE;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CallCenterOS.WEB.Controls
{
    public partial class Bubble : System.Web.UI.UserControl
    {
        public Message Message { get; set; }

        public bool IsAdmin { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Message != null)
            {
                var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                try
                {
                    IsAdmin = userManager.IsInRoleAsync(Message.User_Id, "Admin").Result;
                }
                catch
                {
                    IsAdmin = false;
                }
                text.Text = Message.Text;
                date.Text = Message.Date.ToString("dd/MM/yy HH:mm");
            }
        }
    }
}