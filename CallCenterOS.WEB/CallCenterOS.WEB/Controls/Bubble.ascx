﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Bubble.ascx.cs" Inherits="CallCenterOS.WEB.Controls.Bubble" %>
<div class="bubble <%=!IsAdmin?"alt":""%>">
    <div class="txt">
      <%--<p class="name">Name</p>--%>
      <p class="text" runat="server"><asp:Label ID="text" runat="server" Text=""></asp:Label></p>
      <span class="timestamp"><asp:Label ID="date" runat="server" Text=""></asp:Label></span>
    </div>
    <div class="bubble-arrow <%=!IsAdmin?"alt":""%>"></div>     
  </div>
<br />
 
