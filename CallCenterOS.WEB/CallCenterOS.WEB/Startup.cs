﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CallCenterOS.WEB.Startup))]
namespace CallCenterOS.WEB
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
