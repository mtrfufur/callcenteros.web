﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eShop.CORE
{
    public class Order
    {
        public int Id { get; set; }

        ///<summary>
        /// Usuario que crea la incidencia
        ///</summary>

        public ApplicationUser User { get; set; }

        ///<summary>
        /// Identificador del usuario que ha creado la incidencia
        ///</summary>    
        [ForeignKey("User")]
        public string User_Id { get; set; }

        public virtual List<Order> Orders { get; set; }
    }
}
